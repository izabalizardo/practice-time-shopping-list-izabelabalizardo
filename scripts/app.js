document.addEventListener('DOMContentLoaded', function() {
    const list = [];

    const form = document.querySelector('.shopping-form');
    const itemInput = document.querySelector('.shopping-form-item-input');
    


    const quantInput = document.querySelector('.shopping-form-quant-input');
    const incrementButton = document.querySelector('.shopping-form-increment-button');
    const decrementButton = document.querySelector('.shopping-form-decrement-button');
    const items = document.querySelector(".shopping-items");

    incrementButton.addEventListener("click", incrementQuant);
    decrementButton.addEventListener("click", decrementQuant);

    form.addEventListener("submit", addItemToList);

    function incrementQuant() {
        const currentValue = Number(quantInput.value);
        const newValue = currentValue + 1;

        quantInput.value = newValue;

    }


    function decrementQuant() {
        const currentValue = Number(quantInput.value);
        const newValue = currentValue - 1;

        if (newValue > 0){
            quantInput.value = newValue;

        }

    }

        function addItemToList(event) {
            event.preventDefault();
            
            
            const itemName = event.target["item-name"].value;
            const itemQuant = event.target["item-quant"].value;

            if(itemName != "" ) {

                const item = {
                    name: itemName,
                    quant: itemQuant,
    
                };
    
                list.push(item);
                renderListItems();
                resetInputs();
    
            }
    
        }
    
            function renderListItems(){
    
                let itemsStructure = "";
    
                list.forEach(function (item) {
                    itemsStructure += `
                    
                    <li class="shopping-item">
                    <span>${item.name}</span>
                    <span>${item.quant}</span>
    
                    </li>
                    `;
    
                    
                });

                items.innerHTML = itemsStructure;
            }
                

            function resetInputs() {
                itemInput.value = "";
                quantInput.value = 1;
            }
    });